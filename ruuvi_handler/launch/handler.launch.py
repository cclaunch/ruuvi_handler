
import os

from launch import LaunchDescription
from launch_ros.actions import Node
from ament_index_python.packages import get_package_share_directory


def generate_launch_description():

    share_directory = get_package_share_directory('ruuvi_handler')
    start_js_file = os.path.join(share_directory, 'dist', 'handler.js')
    start_javascript_node = Node(
        name='ruuvi_handler',
        executable='node',
        output='screen',
        arguments=[
            start_js_file
        ],
        cwd=share_directory)

    ld = LaunchDescription()
    ld.add_action(start_javascript_node)

    return ld
