const noble = require("@abandonware/noble");
const rclnodejs = require("rclnodejs");
const fs = require("fs");
const yaml = require("js-yaml");

var publisher = undefined;
var node = undefined;
var ruuvi_config = undefined;

// Set up ROS items
rclnodejs
  .init()
  .then(() => {
    let fileContents = fs.readFileSync("/etc/ruuvi_handler/ruuvi_config.yaml", "utf8");
    ruuvi_config = yaml.load(fileContents);
    console.log(ruuvi_config);
    rclnodejs.regenerateAll();
    node = new rclnodejs.Node("ruuvi_publisher");
    publisher = node.createPublisher("ruuvi_msgs/msg/RuuviTag", ruuvi_config.ros_topic);
    node.spin();
  })
  .catch((error) => {
    console.log(error);
    process.exit(1);
  });

// This will usually fire immediately but we can't do Bluetooth things until it does.
noble.on("stateChange", async (state) => {
  if (state === "poweredOn") {
    try {
      await noble.startScanningAsync([], true);
    } catch (error) {
      console.error(error);
      process.exit(1);
    }
  }
});

// This fires for every Bluetooth device found (Ruuvi or not).
noble.on("discover", async (peripheral) => {
  let mfrIdStart = 0;
  let mfrIdEnd = 4;
  let ruuviTagId = "9904";
  if (peripheral.advertisement.manufacturerData == undefined) {
    return;
  }
  let manufacturerDataString = peripheral.advertisement.manufacturerData.toString("hex");
  if (manufacturerDataString.substring(mfrIdStart, mfrIdEnd) == ruuviTagId) {
    let ruuviData = undefined;
    try {
      ruuviData = parseRuuviData(manufacturerDataString);
    } catch (error) {
      console.log(error);
      return;
    }
    if (peripheral.rssi !== undefined) {
      ruuviData.rssi = peripheral.rssi;
    }
    if (peripheral.address !== undefined) {
      ruuviData.address = peripheral.address;
    }
    const ruuvi_msg = rclnodejs.createMessageObject("ruuvi_msgs/msg/RuuviTag");
    ruuvi_msg.header.frame_id = "unknown";

    for (let i = 0; i < ruuvi_config.ruuvi_tags.length; i++) {
      if (ruuvi_config.ruuvi_tags[i].address === ruuviData.address) {
        ruuvi_msg.header.frame_id = ruuvi_config.ruuvi_tags[i].name;
      }
    }
    if (ruuvi_config[ruuviData.address] !== undefined) {
      ruuvi_msg.header.frame_id = ruuvi_config[ruuviData.address];
    } else {
    }
    let current_time = node.now().secondsAndNanoseconds;
    ruuvi_msg.header.stamp.sec = current_time.seconds;
    ruuvi_msg.header.stamp.nanosec = current_time.nanoseconds;
    ruuvi_msg.address = ruuviData.address;
    ruuvi_msg.format = ruuviData.ruuviformat;
    ruuvi_msg.temperature_c = ruuviData.temperature;
    ruuvi_msg.humidity = ruuviData.humidity;
    ruuvi_msg.pressure_pa = ruuviData.pressure;
    ruuvi_msg.acceleration_x_mg = ruuviData.accelerationX;
    ruuvi_msg.acceleration_y_mg = ruuviData.accelerationY;
    ruuvi_msg.acceleration_z_mg = ruuviData.accelerationZ;
    ruuvi_msg.battery_mv = ruuviData.battery;
    if (ruuviData.ruuviformat === 2) {
      ruuvi_msg.tx_power_dbm = ruuviData.txPower;
      ruuvi_msg.movement_counter = ruuviData.movementCounter;
      ruuvi_msg.sequence_counter = ruuviData.sequenceCounter;
      ruuvi_msg.rssi = ruuviData.rssi;
    } else {
      ruuvi_msg.tx_power_dbm = 0;
      ruuvi_msg.movement_counter = 0;
      ruuvi_msg.sequence_counter = 0;
      ruuvi_msg.rssi = 0;
    }
    publisher.publish(ruuvi_msg);
  }
});

var parseRuuviData = function (manufacturerDataString) {
  let formatStart = 4;
  let formatEnd = 6;
  let formatRawV1 = "03";
  let formatRawV2 = "05";
  let dataFormat = manufacturerDataString.substring(formatStart, formatEnd);
  let dataObject = {};
  switch (dataFormat) {
    case formatRawV1:
      dataObject = parseRawV1Ruuvi(manufacturerDataString);
      dataObject.ruuviformat = 1;
      break;
    case formatRawV2:
      dataObject = parseRawV2Ruuvi(manufacturerDataString);
      dataObject.ruuviformat = 2;
      break;
    default:
      throw new Error("Unsupported Ruuvi format: {0}".format(dataFormat));
  }

  return dataObject;
};

// https://github.com/ruuvi/ruuvi-sensor-protocols
var parseRawV1Ruuvi = function (manufacturerDataString) {
  let humidityStart = 6;
  let humidityEnd = 8;
  let temperatureStart = 8;
  let temperatureEnd = 12;
  let pressureStart = 12;
  let pressureEnd = 16;
  let accelerationXStart = 16;
  let accelerationXEnd = 20;
  let accelerationYStart = 20;
  let accelerationYEnd = 24;
  let accelerationZStart = 24;
  let accelerationZEnd = 28;
  let batteryStart = 28;
  let batteryEnd = 32;

  let robject = {};

  let humidity = manufacturerDataString.substring(humidityStart, humidityEnd);
  humidity = parseInt(humidity, 16);
  humidity /= 2; // scale
  robject.humidity = humidity;

  let temperatureString = manufacturerDataString.substring(temperatureStart, temperatureEnd);
  let temperature = parseInt(temperatureString.substring(0, 2), 16); // full degrees
  temperature += parseInt(temperatureString.substring(2, 4), 16) / 100; // decimals
  if (temperature > 128) {
    // Ruuvi format, sign bit + value
    temperature = temperature - 128;
    temperature = 0 - temperature;
  }
  robject.temperature = +temperature;

  let pressure = parseInt(manufacturerDataString.substring(pressureStart, pressureEnd), 16); // uint16_t pascals
  pressure += 50000; // Ruuvi format
  robject.pressure = pressure;

  let accelerationX = parseInt(manufacturerDataString.substring(accelerationXStart, accelerationXEnd), 16); // milli-g
  if (accelerationX > 32767) {
    accelerationX -= 65536;
  } // two's complement

  let accelerationY = parseInt(manufacturerDataString.substring(accelerationYStart, accelerationYEnd), 16); // milli-g
  if (accelerationY > 32767) {
    accelerationY -= 65536;
  } // two's complement

  let accelerationZ = parseInt(manufacturerDataString.substring(accelerationZStart, accelerationZEnd), 16); // milli-g
  if (accelerationZ > 32767) {
    accelerationZ -= 65536;
  } // two's complement

  robject.accelerationX = accelerationX;
  robject.accelerationY = accelerationY;
  robject.accelerationZ = accelerationZ;

  let battery = parseInt(manufacturerDataString.substring(batteryStart, batteryEnd), 16); // milli-g
  robject.battery = battery;

  return robject;
};

var parseRawV2Ruuvi = function (manufacturerDataString) {
  let temperatureStart = 6;
  let temperatureEnd = 10;
  let humidityStart = 10;
  let humidityEnd = 14;
  let pressureStart = 14;
  let pressureEnd = 18;
  let accelerationXStart = 18;
  let accelerationXEnd = 22;
  let accelerationYStart = 22;
  let accelerationYEnd = 26;
  let accelerationZStart = 26;
  let accelerationZEnd = 30;
  let powerInfoStart = 30;
  let powerInfoEnd = 34;
  let movementCounterStart = 34;
  let movementCounterEnd = 36;
  let sequenceCounterStart = 36;
  let sequenceCounterEnd = 40;

  let robject = {};

  let temperatureString = manufacturerDataString.substring(temperatureStart, temperatureEnd);
  let temperature = parseInt(temperatureString, 16);

  if ((temperature & 0x8000) > 0) {
    temperature = temperature - 0x10000;
  } // two's complement

  robject.temperature = +(temperature / 200); // 0.005 degrees

  let humidityString = manufacturerDataString.substring(humidityStart, humidityEnd);
  let humidity = parseInt(humidityString, 16); // 0.0025%
  robject.humidity = +(humidity / 400);

  let pressure = parseInt(manufacturerDataString.substring(pressureStart, pressureEnd), 16); // uint16_t pascals
  pressure += 50000; //Ruuvi format
  robject.pressure = pressure;

  // acceleration values in milli-Gs
  let accelerationX = parseInt(manufacturerDataString.substring(accelerationXStart, accelerationXEnd), 16); // milli-g
  if ((accelerationX & 0x8000) > 0) {
    accelerationX -= 0x10000;
  } // two's complement

  let accelerationY = parseInt(manufacturerDataString.substring(accelerationYStart, accelerationYEnd), 16); // milli-g
  if ((accelerationY & 0x8000) > 0) {
    accelerationY -= 0x10000;
  } // two's complement

  let accelerationZ = parseInt(manufacturerDataString.substring(accelerationZStart, accelerationZEnd), 16); // milli-g
  if ((accelerationZ & 0x8000) > 0) {
    accelerationZ -= 0x10000;
  } // two's complement

  robject.accelerationX = accelerationX;
  robject.accelerationY = accelerationY;
  robject.accelerationZ = accelerationZ;

  let powerInfoString = manufacturerDataString.substring(powerInfoStart, powerInfoEnd);
  let battery = (parseInt(powerInfoString, 16) >> 5) + 1600; // millivolts > 1600
  let txpower = (parseInt(powerInfoString, 16) & 0x001f) - 40; // dB > -40
  robject.battery = battery;
  robject.txPower = txpower;

  let movementCounterString = manufacturerDataString.substring(movementCounterStart, movementCounterEnd);
  let movementCounter = parseInt(movementCounterString, 16);
  robject.movementCounter = movementCounter;

  let sequenceCounterString = manufacturerDataString.substring(sequenceCounterStart, sequenceCounterEnd);
  let sequenceCounter = parseInt(sequenceCounterString, 16);
  robject.sequenceCounter = sequenceCounter;

  return robject;
};
