#!/bin/bash

set -e

service dbus start
bluetoothd &
source /root/ruuvi_ws/install/setup.bash

exec "$@"
