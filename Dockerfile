FROM ros:humble
ENV DEBIAN_FRONTEND noninteractive
COPY ./entrypoint.sh /
COPY ./healthcheck.sh /
COPY ./ruuvi_handler /root/ruuvi_ws/src/ruuvi_handler
SHELL ["/bin/bash", "-c"]
RUN mkdir -p /root/ruuvi_ws/src
RUN apt-get update && \
    apt-get install -y \
      curl
RUN curl -fsSL https://deb.nodesource.com/setup_18.x | bash - && \
    apt-get update && \
    apt-get install -y \
      bluez \
      bluetooth \
      nodejs
RUN source /opt/ros/humble/setup.bash && \
    npm install -g \
      rclnodejs \
      rclnodejs-cli
RUN cd /root/ruuvi_ws/src && \
    git clone https://gitlab.com/cclaunch/ruuvi_msgs.git
RUN rm -rf /var/lib/apt/lists/*
RUN source /opt/ros/humble/setup.bash && \
    cd /root/ruuvi_ws && \
    colcon build
ENTRYPOINT ["/entrypoint.sh"]
CMD ["ros2", "launch", "ruuvi_handler", "handler.launch.py"]
