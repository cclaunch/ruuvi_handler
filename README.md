# Ruuvi Handler

Ruuvi Handler is a dockerized application which reads Ruuvi environment sensors (a.k.a. Tags) and publishes their data on a ROS topic.

# Hardware Setup

* Bluetooth Low Energy capable Linux computer (tested on a Raspberry Pi 4).

# Host dependencies

* Docker & Docker Compose

# Configuration

The `ruuvi_config.yaml` file contains mappings (the `ruuvi_tags` section) of Ruuvi tag bluetooth MAC addresses to nice names, which get stored in the message header's `frame_id` field.  This config file also has a `ros_topic` field which defines the output ROS topic name.

# Running

To run the dockerized app, simply bringup the compose file from this repo's root directory:

    docker-compose up -d

# Output

The handler outputs Ruuvi messages on a ROS topic named per the field in the `ruuvi_config.yaml`, with the format (depending on whether your tags support the v1 or v2 data standard, both are handled).  Example output:

    ---
    header:
      stamp:
        sec: 1646457764
        nanosec: 792242518
      frame_id: Garage
    address: aa:bb:cc:dd:ee:ff
    format: 2
    temperature_c: 20.290000915527344
    humidity: 64.88999938964844
    pressure_pa: 101804
    acceleration_x_mg: 744
    acceleration_y_mg: -708
    acceleration_z_mg: 28
    battery_mv: 2863
    tx_power_dbm: -18
    movement_counter: 99
    sequence_counter: 44359
    rssi: -83

# How It Works

Ruuvi Handler uses a Javascript library called [noble](https://www.npmjs.com/package/@abandonware/noble) to read the Bluetooth advertising data of every Bluetooth device in range of the host's default bluetooth interface device (typically `hci0`).  The handler filters out devices based on if the advertising data matches that of a Ruuvi Tag.  If so, it parses the tag's data.  Then a library called [rclnodejs](https://github.com/RobotWebTools/rclnodejs) is used to create and publish to a ROS topic.  The docker is launched using `network=host` in order for it to have full access to the host's bluetooth stack.
