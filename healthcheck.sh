#!/bin/bash

set -e

source /root/ruuvi_ws/install/setup.bash
timeout 5 ros2 topic echo --once /ruuvi_handler/sensors
